<?php

/**
 * Plugin Name: ALC API Form
 * Plugin URI: http://www.australianlendingcentre.com.au
 * Description: ALC API Enquire Form for Money Maker
 * Version: 1.0
 * Author: Tim
 * Author URI: http://www.loong.com.au
 * License: ALC Copy Right, All Rights Reserved.
 */
function register_alcapiform_session() {
    if (!session_id())
        session_start();

    if (!empty($_SERVER['HTTP_REFERER'])) {
        //if the referer is website itself, then ignore. otherwise, go ahead.
        if (!TimStartsWith($_SERVER['HTTP_REFERER'], 'http://' . $_SERVER['SERVER_NAME']) &&
                !TimStartsWith($_SERVER['HTTP_REFERER'], 'https://' . $_SERVER['SERVER_NAME'])) {
            $_SESSION['mm_referalurl'] = $_SERVER['HTTP_REFERER'];
        }
    }

    if (isset($_REQUEST['t'])) {
        $_SESSION['mm_adwords_t'] = $_REQUEST['t'];
    }
    if (isset($_REQUEST['k'])) {
        $_SESSION['mm_adwords_k'] = $_REQUEST['k'];
    }
    if (isset($_REQUEST['a'])) {
        $_SESSION['mm_adwords_a'] = $_REQUEST['a'];
    }
    if (isset($_REQUEST['ref'])) {
        $_SESSION['mm_adwords_ref'] = $_REQUEST['ref'];
    }
	if (isset($_REQUEST['ne'])) {
        $_SESSION['mm_new_enquiry'] = $_REQUEST['ne'];
    }
}

add_action('init', 'register_alcapiform_session');

function alcapiform_shortcode_api_contact_form($atts, $content = null) {

    //load jquery if not loaded
    if (!wp_script_is('jquery')) {
        wp_enqueue_script('jquery');
    }

    //detect platform
    $platform = 'desktop';
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
        $platform = 'mobile';

    //echo $useragent.'|'.$platform;

    $contact_form = "";

    $options = get_option('alcapiform_option_name');

    //load company ID - required
    $_cid = isset($atts['cid']) ? $atts['cid'] : $options['cid'];
    if (empty($_cid)){
     //   $_actualURL = $_SERVER['SERVER_NAME'];

        $XML = '<?xml version="1.0" encoding="UTF-8"?>
        <pingdom_http_custom_check>
                    <status>Not Ok</status>
                    <response_time>96.777</response_time>
                   </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

        $to = 'jun@chillidee.com.au';
        $subject = 'Some Error Happend in your website:'. $_SERVER['SERVER_NAME'] .'';
        $body = 'Error: CID is not set.';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent =  wp_mail( $to, $subject, $body, $headers );
                        if (!$sent): 
                            return 'Email Not sent';
                        endif;
        return 'Error: CID is not set. ';
    }

    //load API GET URL - required
    $_geturl = isset($atts['geturl']) ? $atts['geturl'] : $options['geturl'];
    if (empty($_geturl)){
        $XML = '<?xml version="1.0" encoding="UTF-8"?>
        <pingdom_http_custom_check>
                    <status>Not Ok</status>
                    <response_time>96.777</response_time>
                   </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

        $to = 'jun@chillidee.com.au';
        $subject = 'Error: API GET URL is not set. ';
        $body = 'The Error happend in '. $_SERVER['SERVER_NAME'] .'';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent =  wp_mail( $to, $subject, $body, $headers );
                        if (!$sent): 
                            return 'Error: API GET URL is not set. ';
                        endif;

        return 'Error: API GET URL is not set. ';
    }
    if (substr($_geturl, -1) != '/')
        $_geturl .= '/';

    //load API POST URL - required
    $_posturl = isset($atts['posturl']) ? $atts['posturl'] : $options['posturl'];
    if (empty($_posturl)) {
        $XML = '<?xml version="1.0" encoding="UTF-8"?>
        <pingdom_http_custom_check>
                    <status>Not Ok</status>
                    <response_time>96.777</response_time>
                   </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

        $to = 'jun@chillidee.com.au';
        $subject = 'Error: API POST URL is not set. ';
        $body = 'The Error happend in '. $_SERVER['SERVER_NAME'] .'';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $sent =  wp_mail( $to, $subject, $body, $headers );
                        if (!$sent): 
                            return 'Error: API POST URL is not set. ';
                        endif;

        return 'Error: API POST URL is not set. ';
    }

    $XML = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>
        <pingdom_http_custom_check>
            <status>OK</status>
            <response_time>96.777</response_time>
        </pingdom_http_custom_check>';
        echo '<pre style="display:none;">' . $XML . '</pre>';

    //load enquiry form css file
    $_css = isset($atts['css']) ? $atts['css'] : $options['css'];
    if (!empty($_css))
        $contact_form .= '<link href="' . $_css . '" rel="stylesheet" type="text/css" />';

    $_privacy = isset($atts['privacy']) ? $atts['privacy'] : $options['privacy'];
    if (!empty($_privacy))
        $_privacy = '/privacy-policy/';

    $_psurl = isset($atts['pssurl']) ? $atts['pssurl'] : $options['pssurl'];

    $_partner_email = isset($atts['partner_email']) ? $atts['partner_email'] : $options['partner_email'];

    $_from_email = isset($atts['from_email']) ? $atts['from_email'] : $options['from_email'];

    $company = isset($atts['company_name']) ? $atts['company_name'] : $options['company_name'];

    $_privacy_link_text = isset($atts['privacy_link_text']) ? $atts['privacy_link_text'] : $options['privacy_link_text'];

    $_privacy_text = isset($atts['privacy_text']) ? $atts['privacy_text'] : $options['privacy_text'];

    $_sendemails = isset($atts['send_emails']) ? $atts['send_emails'] : $options['send_emails'];

    //load jquery if specified
    $_jquery = isset($atts['jquery']) ? $atts['jquery'] : $options['jquery'];
    if (!empty($_jquery))
        $contact_form .= '<script src="' . $_jquery . '"></script>';

    //do form submission
    if (isset($_POST['alcapiformsumbit'])) {
        //check spam bot
        if (!isset($_POST['website_url']) || empty($_POST['website_url'])) {

            $data = array();
            $data['cid'] = strtolower($_cid);
            $data['title'] = isset($_POST['title']) ? $_POST['title'] : "";
            $data['firstName'] = isset($_POST['firstName']) ? $_POST['firstName'] : null;
            $data['lastName'] = isset($_POST['lastName']) ? $_POST['lastName'] : null;
            $data['loanAmount'] = isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
            $data['typeOfLoan'] = $_POST['typeOfLoan'];
            $data['hasProperty'] = (isset($_POST['hasProperty']) && $_POST['hasProperty'] == 1) ? "true" : "false";
            $data['haveDeposit'] = (isset($_POST['haveDeposit']) && $_POST['haveDeposit'] == 1) ? "true" : "false";
            $data['realEstateValue'] = (isset($_POST['realEstateValue']) && !empty($_POST['realEstateValue'])) ? str_replace(array('$', ','), '', $_POST['realEstateValue']) : 0;
            $data['balanceOwing'] = (isset($_POST['balanceOwing']) && !empty($_POST['balanceOwing'])) ? str_replace(array('$', ','), '', $_POST['balanceOwing']) : 0;
            $data['mobileNumber'] = $_POST['mobileNumber'];
           // $data['landLineNumber'] = (isset($_POST['landLineNumber']) && !empty($_POST['landLineNumber'])) ? $_POST['landLineNumberAreaCode'] . $_POST['landLineNumber'] : '';
            $data['emailAddress'] = $_POST['emailAddress'];
            $data['state'] = $_POST['state'];
            //$data['suburb'] = $_POST['suburb'];
//          $data['postCode'] = $_POST['postCode'];
            if (!empty($_POST['field1']) || !empty($_POST['field2']) ) {
              $data['suburb'] = $_POST['field1'];
                $data['postCode'] = $_POST['field2'];
           }
             else{
               $data['suburb'] = 'none';
               $data['postCode'] = $_POST['postCode'];
           }
         //   $data['suburb'] = $_POST['field1'];
          //  $data['postCode'] = $_POST['field2'];
            $data['referral'] = isset($_POST['referral']) ? $_POST['referral'] : isset($_SESSION['mm_adwords_ref']) ? $_SESSION['mm_adwords_ref'] : '';
            
            $comments2 .= 'Customer comments: ' . $_POST['comments'] . ' <br> ';            

            //Judgments or defaults			   
            $answer = ($_POST['recentjudge'] == Yes) ? "Yes" : "No";
            $comments2 .= '- Any judgments or defaults? - ' . $answer . ' <br> ';
            
            $questionsanswers .= "<br>Any judgments or defaults: ";
            $questionsanswers .= ($_POST['recentjudge'] == Yes) ? "Y" : "N";
            $answers .= ($_POST['recentjudge'] == Yes) ? "Y" : "N";
            
            if($_POST['recentjudge'] == Yes) {
                //Paid or unpaid	
                $answer	= ($_POST['bepaid'] == Yes) ? "Yes" : "No";
                $comments2 .= '- Paid or unpaid? - ' . $answer . ' <br> ';
                
                $questionsanswers .= "<br>Paid or unpaid: ";
                $questionsanswers .= ($_POST['bepaid'] == Yes) ? "Y" : "N";
                $answers .= ($_POST['bepaid'] == Yes) ? "Y" : "N";
            }

			if(isset($_POST['prefCallBack'])) {			
				$comments2 = '<br>Preferred time for call back: ' . $_POST['prefCallBack'] . '<br>________________________________________<br><br>' . $comments2;			
			}
			$data['comments'] = $comments2;
			
            $data['referrer'] = urlencode($_SESSION['mm_referalurl']);
            $data['t'] = $_SESSION['mm_adwords_t'];
            $data['k'] = $_SESSION['mm_adwords_k'];
            $data['a'] = $_SESSION['mm_adwords_a'];
            $data['platform'] = $platform;
            $data['UserHasDefaults'] = $_POST['recentjudge'];

            if(isset($_SESSION['mm_new_enquiry']) && $_SESSION['mm_new_enquiry']){
				$data['LenderID'] = '';
				$data['CallStatusID'] = '14';				
			}

            //echo "<pre>"; print_r($data); echo "</pre>";
            // Setup cURL
            $data_string = json_encode($data);

            $ch = curl_init($_posturl);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
            );

            // Send the request
            $response = curl_exec($ch);
			
			date_default_timezone_set('Australia/Melbourne');

            // Check for errors
            if ($response === FALSE) {
				
				// Record lead in log
				record_lead($_cid, array($data['firstName'], $data['loanAmount'], $data['typeOfLoan'], $data['mobileNumber'], $data['emailAddress'], $platform, date("Y-m-d H:i:s"), "NO RESPONSE", curl_error($ch), $data_string));
				
                if (isset($_SESSION['failureURL']) && $_SESSION['failureURL'] != '') {
                    die('<script type="text/javascript">window.location.href="' . $_SESSION['failureURL'] . '";</script>');
                } else {
                    die(curl_error($ch));
                }
            }

            // Decode the response
            $responseData = json_decode($response, TRUE);

            //echo "<pre>"; print_r($responseData); echo "</pre>";
            //echo "<pre>"; print_r($_POST); echo "</pre>";

            $responseString = "";

            if ($responseData['status'] == 'SUCCESS') {
				
				// Record lead in log
				record_lead($_cid, array($data['firstName'], $data['loanAmount'], $data['typeOfLoan'], $data['mobileNumber'], $data['emailAddress'], $platform, date("Y-m-d H:i:s"), $responseData['status'], 'No', $data_string));
								
                $responseString = "<p class='api-post-success'>Successful</p>";
                if ((isset($_sendemails) && $_sendemails == 'on') && ($data['hasProperty'] == 'false' && $data['typeOfLoan'] == '176')):
                    $headers = array();
                    array_push($headers, 'From: ' . $company . ' <' . $_from_email . '>');
                    array_push($headers, 'Content-Type: text/html; charset=UTF-8');
                    if (isset($_partner_email) && !empty($_partner_email)):
                        $partner_email_html = '<h1>Applicant Details</h1>';
                        if ($data['loanAmount'])
                            $partner_email_html .= '<p>Loan Amount: $' . $data['loanAmount'] . '</p><br>';
                        $partner_email_html .= '<p>Name: ';
                        if ($data['title'])
                            $partner_email_html .= $data['title'] . ' ';
                        if ($data['firstName'])
                            $partner_email_html .= $data['firstName'] . ' ';
                        if ($data['lastName'])
                            $partner_email_html .= $data['lastName'] . ' ';
                        $partner_email_html .= '</p><br>';
                        if ($data['mobileNumber'])
                            $partner_email_html .= '<p>Phone: ' . $data['mobileNumber'] . '</p><br>';
                        if ($data['emailAddress'])
                            $partner_email_html .= '<p>Email: ' . $data['emailAddress'] . '</p><br>';
                        if ($data['suburb'])
                            $partner_email_html .= '<p>Suburb: ' . $data['suburb'] . '</p><br>';
                        if ($data['state'])
                            $partner_email_html .= '<p>State: ' . $data['state'] . '</p><br>';
                        if ($data['postCode'])
                            $partner_email_html .= '<p>Post Code: ' . $data['postCode'] . '</p><br>';
                        if ($data['comments'])
                            $partner_email_html .= '<p>Comments: ' . $data['comments'] . '</p><br>';
                        $sent = wp_mail($_partner_email, 'New Lead', $partner_email_html, $headers);
                        if (!$sent):
                         wp_mail('info@chillidee.com.au', 'Alert! Customer details not sent to partner', $partner_email_html, $headers);
                        endif;
                    endif;

                    $thanksEmail = file_get_contents(plugin_dir_path(__FILE__) . 'emails/' . $data['cid'] . '-thanks-email.html');
                    if ($thanksEmail && $data['emailAddress']):
                        wp_mail($data['emailAddress'], $company, $thanksEmail, $headers);
                    endif;


                    if (isset($_psurl) && $_psurl != ''):
                        $now    = new DateTime();
                        $future = new DateTime("2019-01-01 00:00:00");
                    
                        if($now < $future) {
                            die('<script type="text/javascript">window.location.href="'.home_url().'/complete' . getUrlParams() . '";</script>');
                        } else {
                            die('<script type="text/javascript">window.location.href="' . $_psurl . '";</script>');
                        } 
                    else:
                        if (isset($_SESSION['sucessURL']) && $_SESSION['sucessURL'] != '') {
                            die('<script type="text/javascript">window.location.href="' . $_SESSION['sucessURL'] . '";</script>');
                        }
                    endif;

                else:
                    {
                      die('<script type="text/javascript">window.location.href="https://australiandebtagreements.com.au/complete' . getUrlParams() . '";</script>');
                    }
                endif; 
            } else {
				
				// Record lead in log
				record_lead($_cid, array($data['firstName'], $data['loanAmount'], $data['typeOfLoan'], $data['mobileNumber'], $data['emailAddress'], $platform, date("Y-m-d H:i:s"), $responseData['status'], implode (", ", $responseData['errorMessage']), $data_string));
								
                $responseString = "<p class='api-post-failed'><ul>";
                foreach ($responseData['errorMessage'] as $value) {
                    $responseString .= "<li>" . htmlspecialchars($value) . "</li>";
                }
                $responseString .= "</ul></p>";
            }
        }
    }

    $jsonurl = $_geturl . $platform . '/' . strtolower($_cid);
    $json = file_get_contents($jsonurl);
    $json = json_decode($json, true);

    //echo "<pre>"; print_r($json); echo "</pre>";

    $_SESSION['sucessURL'] = $json['sucessURL'];
    $_SESSION['failureURL'] = $json['failureURL'];

    $typeOfLoan_options = '<option value="" selected="selected">Share your plans...</option>';
    if (isset($json['typeOfLoan'])) {
        foreach ($json['typeOfLoan'] as $key => $value) {
            $typeOfLoan_options .= '<option value="' . $key . '">' . $value . '</option>';
        }
    }

    $contact_form .= '<form id="apiEnquiryForm" name="apiEnquiryForm" action="" method="post">';


    if (!empty($responseString))
        $contact_form .= $responseString;

    if (in_array('loanAmount', $json['fields'])) {
        $contact_form .= '<fieldset><p>
            <label for="loanAmount">How much do you need?</label>
            <input id="loanAmount" type="text" name="loanAmount" placeholder="Tell us how much..." />
            <span class="alcerror" id="loanAmountError" style="color: red; display:none;"></span>
        </p>';
    }

  if (in_array('referral', $json['fields'])) {
        $contact_form .= '<p>
    <label for="referral">Where did you hear about us?</label>
    <select name="referral" id="referral" class="referral">
        <option selected="selected" value="">[Select One]</option>
        <option value="Yahoo">Yahoo</option>
        <option valustate2e="Radio">Radio</option>
        <option value="Newspaper">Newspaper</option>
        <option value="Google">Google</option>
        <option value="Television">Television</option>
        <option value="Family/Friend">Family/Friend</option>
        <option value="Email">Email</option>
        <option value="Other/Not Sure">Other/Not Sure</option>
    </select>
    <span class="alcerror" id="referralError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('typeOfLoan', $json['fields'])) {
        $contact_form .= '<p>
        <label for="typeOfLoan">What do you need it for?</label>
        <select id="typeOfLoan" name="typeOfLoan">
          ' . $typeOfLoan_options . '
      </select>
      <span class="alcerror" id="typeOfLoanError" style="color: red; display:none;"></span>
  </p>';
    }

    if (in_array('hasProperty', $json['fields'])) {
        $contact_form .= '<p>
        <div class="switch-field">
            <div class="switch-title" style="display: inline-block;vertical-align: middle;">Do you own your home?</div>
            <div class="switch-buttons">
                <div class="switch-buttons-yes">
                    <input type="radio" id="hasPropertyYes" name="hasProperty" value="1" required></input>
                    <label for="hasPropertyYes" class="radio">Yes</label>
                </div>
                <div class="switch-buttons-no">
                    <input type="radio" id="hasPropertyNo" name="hasProperty" value="0" required></input>
                    <label for="hasPropertyNo" class="radio">No</label>
                </div>            
            </div>
            <p><span class="alcerror" id="hasPropertyError" style="color: red; display:none;"></span></p>
        </div>
    </p>';

        $contact_form .= '<div id="yourPropertySection" class="yourPropertySection" style="display:none;">
                            <p>Tell us a little about your property</p>
                            <p>
                            <label for="realEstateValue">Total real estate value ($)</label>
                            <input id="realEstateValue" type="text" name="realEstateValue" />
                            <span class="alcerror" id="realEstateValueError" style="color: red; display:none;"></span>
                            </p>
                            <p>
                            <label for="balanceOwing">Balance Owing ($)</label>
                            <input id="balanceOwing" type="text" name="balanceOwing" />
                            <span class="alcerror" id="balanceOwingError" style="color: red; display:none;"></span>
                            </p>
                            </div>';


        $contact_form .= '<p>
                            <div id="judgmentsDefaultsSection" style="display:none;" class="switch-field">
                                <div class="switch-title" style="display: inline-block;vertical-align: middle;">Do you have any recent judgments or defaults on your credit report?</div>
                                <div class="switch-buttons">
                                    <div class="switch-buttons-yes">
                                        <input type="radio" id="recentjudgeyes" name="recentjudge" value="Yes" required></input>
                                        <label for="recentjudgeyes" class="radio">Yes</label>
                                    </div>
                                    <div class="switch-buttons-no">
                                        <input type="radio" id="recentjudgeno" name="recentjudge" value="No" required></input>
                                        <label for="recentjudgeno" class="radio">No</label>
                                    </div>            
                                </div>                             
                            </div>
                        </p>';

        $contact_form .= '<p>
                            <div id="paidSection" style="display:none;" class="switch-field">
                                <div class="switch-title" style="display: inline-block;vertical-align: middle;">Are they paid or unpaid?</div>
                                <div class="switch-buttons">
                                    <div class="switch-buttons-yes">
                                        <input type="radio" id="paidyes" name="bepaid" value="1" required></input>
                                        <label for="paidyes" class="radio">Paid</label>
                                    </div>
                                    <div class="switch-buttons-no">
                                        <input type="radio" id="paidno" name="bepaid" value="0" required></input>
                                        <label for="paidno" class="radio">Unpaid</label>
                                    </div>            
                                </div>                       
                            </div>
                        </p>';
    }
    $contact_form .= '<input type="button" name="next0" class="next0 btn btn-info" value="Let\'s Get Started!"/><p id="wrongmessage" style="font-size: 16px!important;font-weight:700;color: #a82d2d !important;"></fieldset>';
    /*
      if(in_array('haveDeposit', $json['fields'])){
      $contact_form .= '<p>
      <label>Do you have a minimum 20% deposit of the purchase price?*</label>
      <input type="radio" id="haveDepositYes" name="haveDeposit" value="1" required></input>
      <label for="haveDepositYes" class="radio">Yes</label>
      <input type="radio" id="haveDepositNo" name="haveDeposit" value="0" required></input>
      <label for="haveDepositNo" class="radio">No</label>
      <span class="alcerror" id="haveDepositError" style="color: red; display:none;"></span>
      </p>';
      } */

    if (in_array('title', $json['fields'])) {
        $contact_form .= '<p>
    <label>Title</label>
    <input type="radio" id="titleMr" name="title" value="Mr"></input>
    <label for="titleMr" class="radio">Mr</label>
    <input type="radio" id="titleMrs" name="title" value="Mrs"></input>
    <label for="titleMrs" class="radio">Mrs</label>
    <input type="radio" id="titleMs" name="title" value="Ms"></input>
    <label for="titleMs" class="radio">Ms</label>
    <input type="radio" id="titleDr" name="title" value="Dr"></input>
    <label for="titleDr" class="radio">Dr</label>
    <span class="alcerror" id="titleError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('firstName', $json['fields'])) {
        $contact_form .= '<fieldset> <p>
    <label for="firstName">First Name</label>
    <input id="firstName" type="text" name="firstName" />
    <span class="alcerror" id="firstNameError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('lastName', $json['fields'])) {
        $contact_form .= '<p>
    <label for="lastName">Last Name</label>
    <input id="lastName" type="text" name="lastName" />
    <span class="alcerror" id="lastNameError" style="color: red; display:none;"></span>
</p>';
    }

    if (in_array('mobileNumber', $json['fields'])) {
        $contact_form .= '<p>
    <label for="mobileNumber">Mobile number</label> 
    <input id="mobileNumber" type="number" min="0" name="mobileNumber" maxlength="10"/>
    <span class="alcerror" id="mobileNumberError" style="color: red; display:none;"></span>
</p>';
    }

    /*if (in_array('landLineNumber', $json['fields'])) {
        $contact_form .= '<p>
    <label for="landLineNumber">Landline number</label>
    <select name="landLineNumberAreaCode" id="landLineNumberAreaCode" class="landLineNumberAreaCode">
        <option selected="selected" value="02">02</option>
        <option value="03">03</option>
        <option value="07">07</option>
        <option value="08">08</option>
    </select>
    <input id="landLineNumber" type="number" min="0" name="landLineNumber" class="landLineNumber" />
    <span class="alcerror" id="landLineNumberError" style="color: red; display:none;"></span>
</p>';
    }*/

    if (in_array('emailAddress', $json['fields'])) {
        $contact_form .= '<p>
    <label for="emailAddress">Email</label>
    <input id="emailAddress" type="email" name="emailAddress" />
    <span class="alcerror" id="emailAddressError" style="color: red; display:none;"></span>
</p>';
    }
	
	/*$contact_form .= '<p>
        <label for="prefCallBack">Preferred time for call back*</label>
        <select id="prefCallBack" name="prefCallBack">
          <option value="Anytime" selected="selected">Anytime</option>
		  <option value="Morning">Morning</option>
		  <option value="Afternoon">Afternoon</option>
		  <option value="Evening">Evening</option>
		  <option value="Weekend">Weekend</option>
      </select>
      <span class="alcerror" id="prefCallBackError" style="color: red; display:none;"></span>
		</p>';*/

    if (in_array('suburb', $json['fields'])) {
        $contact_form .= '<p>
    <label for="suburb" style="display:none;">Suburb</label>
    <span class="alcerror" id="suburbError" style="color: red; display:none;"></span>
</p>';
    }
   
    $contact_form .= '<input type="button" name="next1" class="next1 btn btn-info" value="Next" />';
    $contact_form .= '<input type="button" name="previous" class="previous btn btn-default" value="Previous" /><p id="wrongmessage1" style="font-size: 16px!important;font-weight:700;color: #a82d2d !important;"></fieldset>';


    if (in_array('state', $json['fields'])) {
        $contact_form .= '<fieldset><div class="state-postcode-wrapper"><div style="width: 35%;display: inline-block; margin-top:0; float: left;">
    <p><label for="state">State</label></p>
    <select id="state" name="state" class="state">
        <option value="ACT">ACT</option>
        <option selected="selected" value="NSW">NSW</option>
        <option value="NT">NT</option>
        <option value="QLD">QLD</option>
        <option value="SA">SA</option>
        <option value="TAS">TAS</option>
        <option value="VIC">VIC</option>
        <option value="WA">WA</option>
    </select>
</div>';
    }

    if (in_array('postCode', $json['fields'])) {
        $contact_form .= '<div style="width: 60%;display: inline-block;margin-left: 5%; margin-top:0;">
    <p><label for="postCode">Post Code</label></p>
    <input id="postCode" type="text" name="postCode" class="postcode" />
    <input name="field1" id="field1" type="text" style="display:none;">
    <input name="field2" id="field2" type="text" style="display:none;">
    <input name="field3" id="field3" type="text" style="display:none;">
    <p><span class="alcerror" id="postCodeError" style="color: red; display:none;"></span></p>
</div></div>';
    }

  
    if (in_array('comments', $json['fields'])) {
        $contact_form .= '<p class="comments-section">
    <label for="comments">Comments</label>
    <textarea id="comments" name="comments"></textarea>
</p>';
    }

    $contact_form .= '<input type="text" name="website_url" id="website_url" />';

    $contact_form .= '<div id="error_message"></div>';    

    $contact_form .= '<div class="squaredFour">';
    $contact_form .= '<input type="checkbox" checked id="terms" style="display: none;"/><label for="terms"></label>';
    $contact_form .= '<p>' . $_privacy_text . ' <a href="' . $_privacy . '">' . $_privacy_link_text . '</a></p>';
    $contact_form .= '<span class="alcerror" id="termsError" style="color: red; display:none;margin-top:20px;"></span>';
    $contact_form .= '</div>';

    $contact_form .= '<input type="hidden" name="alcapiformsumbit" value="1" id="alcapiformsumbit">';
    $contact_form .= '<input type="button" name="apiSubmit" value="Submit" id="apiSubmit">';
    $contact_form .= '<input type="button" name="previous" class="previous btn btn-default" value="Previous" /></fieldset>';

    $contact_form .= '</form>';
	$contact_form .= '<div class="nocreaitimapct"><img class="alignnone size-full wp-image-17829" src="/wp-content/uploads/2017/09/no-effect-on-credit-check.png" alt="NO-effect-on-your-credit-file" width="18" height="21" style="margin-right: 10px;">NO effect on your credit file</div>';

    $contact_form .= '<script type="text/javascript">
$(document).ready(function(){   
    var current = 1,current_step,next_step,steps;
    steps = $("fieldset").length;
    $(".next0").click(function(){
        if(document.apiEnquiryForm.loanAmount.value == "" || document.apiEnquiryForm.hasProperty.value == "" ||
           (jQuery("#hasPropertyYes").is(":checked") && document.apiEnquiryForm.realEstateValue.value == "") || (jQuery("#hasPropertyYes").is(":checked") && document.apiEnquiryForm.balanceOwing.value == "")){
           document.getElementById("wrongmessage").innerHTML = "Please Fill Your Information to continue";           
           check_loanAmount();
           check_typeOfLoan();
           check_Property();
           check_PropertyValues();  
        } else if ((document.apiEnquiryForm.recentjudge.value == "") || (jQuery("#recentjudgeyes").is(":checked") && document.apiEnquiryForm.bepaid.value == "")) {
            document.getElementById("wrongmessage").innerHTML = "Please Fill Your Information to continue";         
       }
        else {
        current_step = $(this).parent();
        next_step = $(this).parent().next()
        next_step.show();
        current_step.hide();
        setProgressBar(++current);
        document.getElementById("wrongmessage").innerHTML = "";
    }
    });

    $(".next1").click(function(){
        if( document.apiEnquiryForm.firstName.value == "" || document.apiEnquiryForm.mobileNumber.value == "" || document.apiEnquiryForm.emailAddress.value == ""){
        document.getElementById("wrongmessage1").innerHTML = "Please Fill Your Information to continue";
        check_firstName();
        check_mobileNumber();
        check_emailAddress();
        } else if(jQuery("#mobileNumber").val().indexOf("04") != 0 || jQuery("#mobileNumber").val().length != 10){
        document.getElementById("wrongmessage1").innerHTML = "Invalid Mobile number, please try again";
        check_firstName();
        check_mobileNumber();
        check_emailAddress();
        } else if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(jQuery("#emailAddress").val()) == false){
        document.getElementById("wrongmessage1").innerHTML = "Invalid Email, please try again";
        check_firstName();
        check_mobileNumber();
        check_emailAddress();
       }
        else {
        current_step = $(this).parent();
        next_step = $(this).parent().next()
        next_step.show();
        current_step.hide();
        setProgressBar(++current);
        document.getElementById("wrongmessage1").innerHTML = "";
    }
    });


    $(".previous").click(function(){
        current_step = $(this).parent();
        next_step = $(this).parent().prev();
        next_step.show();
        current_step.hide();
        setProgressBar(--current);
    });
    setProgressBar(current);
    // Change progress bar action
    function setProgressBar(curStep){
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
            .css("width",percent+"%")
            .html(percent+"%");     
    }

    $("#recentjudgeyes").click(function(){
        document.getElementById("paidSection").style.display = "block"; 
     });

     $("#recentjudgeno").click(function(){
        document.getElementById("paidno").checked = false;
        document.getElementById("paidyes").checked = false;
        document.getElementById("paidSection").style.display = "none"; 
     });

    var web_url = document.getElementById("website_url");
    web_url.parentNode.removeChild(web_url);

    var ERROR_1 = "<li>Please tell us if you currently own or paying off real estate.</li>";
    var ERROR_2 = "<li>Please choose a type of loan you\'re interested in.</li>";
    var ERROR_3 = "<li>Loan amount is required.</li>";
    var ERROR_4 = "<li>Total real estate value required.</li>";
    var ERROR_5 = "<li>Balance owing required.</li>";
    var ERROR_6 = "<li>Mobile is required.</li>";
    var ERROR_7 = "<li>Mobile number is incomplete, or not in the correct format.</li>";
    var ERROR_8 = "<li>Email is required.</li>";
    var ERROR_9 = "<li>Invalid email format.</li>";
    var ERROR_10 = "<li>Your PostCode is required.</li>";
    var ERROR_11 = "<li>Postcode is incomplete or invalid.</li>";
    var ERROR_12 = "<li>First name is required.</li>";
    var ERROR_13 = "<li>Last name is required.</li>";
    var ERROR_14 = "<li>Landline number is incomplete, or not in the correct format.</li>";
    var ERROR_15 = "<li>Suburb is required.</li>";
    var ERROR_16 = "<li>Please choose where did you hear about us?</li>";
    var ERROR_17 = "<li>Please tell us if you have a minimum 20% deposit of the purchase price.</li>";
    var ERROR_18 = "<li>Please accept the privacy policy.</li>";

    var error_message = "";

    jQuery("#loanAmount, #realEstateValue, #balanceOwing").blur(function() {
      var value= jQuery(this).val().replace(/[^\d\.]/g, "");
      value = value.trim();
      if(value == ""){
        jQuery(this).val("");

        if(jQuery(this).attr("id") == "loanAmount"){
            check_loanAmount();
        }
        if(jQuery(this).attr("id") == "realEstateValue"){
            check_PropertyValues(true, false);
        }
        if(jQuery(this).attr("id") == "balanceOwing"){
            check_PropertyValues(false, true);
        }

    }else{
        value = Math.round(value) + "";
        value = value.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        jQuery(this).val("$"+value);

        if(jQuery(this).attr("id") == "loanAmount"){
            check_loanAmount();
        }

        if(jQuery(this).attr("id") == "realEstateValue"){
            check_PropertyValues(true, false);
        }

        if(jQuery(this).attr("id") == "balanceOwing"){
            check_PropertyValues(false, true);
        }

        showErrorMessages();
    }
});
jQuery("#loanAmount, #realEstateValue, #balanceOwing").keypress(function(event) {
  if (event.which < 48 || event.which > 57 || jQuery(this).val().length > 8) {
      event.preventDefault();
  }
});
jQuery("#loanAmount, #realEstateValue, #balanceOwing").focusin(function(){
    jQuery(this).val(jQuery(this).val().replace(/[$,]/g, ""));
    jQuery("#judgmentsDefaultsSection").show();
});


jQuery("#mobileNumber").focusin(function(){
    jQuery("#mobileNumberError").hide();
    error_message = error_message.replace(ERROR_6, "");
    error_message = error_message.replace(ERROR_7, "");
    showErrorMessages();
});
jQuery("#mobileNumber").blur(function() {
    var value= jQuery(this).val().replace(/[^\d]/g, "");
    value= value.replace(/^61/, "0");
    jQuery(this).val(value);
    check_mobileNumber();
    showErrorMessages();
});


jQuery("#emailAddress").focusin(function() {
    jQuery("#emailAddressError").hide();
    error_message = error_message.replace(ERROR_8, "");
    error_message = error_message.replace(ERROR_9, "");
    showErrorMessages();
});
jQuery("#emailAddress").blur(function(){
    check_emailAddress();
    showErrorMessages();
});


/*jQuery("#landLineNumber").focusin(function(){
    jQuery("#landLineNumberError").hide();
    error_message = error_message.replace(ERROR_14, "");
    showErrorMessages();
});
jQuery("#landLineNumber").blur(function() {
    var value= jQuery(this).val().replace(/[^\d]/g, "");
    jQuery(this).val(value);
    check_landLineNumber();
    showErrorMessages();
});*/


jQuery("#postCode").focusin(function() {
    jQuery("#postCodeError").hide();
    error_message = error_message.replace(ERROR_10, "");
    error_message = error_message.replace(ERROR_11, "");
    showErrorMessages();
});
jQuery("#postCode").blur(function() {
    var value= jQuery(this).val().replace(/[^\d]/g, "");
    jQuery(this).val(value);
    check_postCode();
    showErrorMessages();
});

jQuery("#firstName").focusin(function() {
    jQuery("#firstNameError").hide();
    error_message = error_message.replace(ERROR_12, "");
    showErrorMessages();
});
jQuery("#firstName").blur(function() {
    var value= jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
    jQuery(this).val(value);
    check_firstName();
    showErrorMessages();
});

jQuery("#lastName").focusin(function() {
    jQuery("#lastNameError").hide();
    error_message = error_message.replace(ERROR_13, "");
    showErrorMessages();
});
jQuery("#lastName").blur(function() {
    var value= jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
    jQuery(this).val(value);
    check_lastName();
    showErrorMessages();
});

jQuery("#suburb").focusin(function() {
    jQuery("#suburbError").hide();
    error_message = error_message.replace(ERROR_15, "");
    showErrorMessages();
});
jQuery("#terms").focusin(function() {
    jQuery("#termsError").hide();
    error_message = error_message.replace(ERROR_18, "");
    showErrorMessages();
});
jQuery("#suburb").blur(function() {
    var value= jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
    jQuery(this).val(value);
    check_suburb();
    showErrorMessages();
});


jQuery("#typeOfLoan").change(function(){
  check_typeOfLoan();
  showErrorMessages();
});

jQuery("#referral").change(function(){
   check_referral();
   showErrorMessages();
});

jQuery("input[type=radio][name=hasProperty]").change(function(){
  check_Property();
  if(this.value == "1"){
     jQuery("#yourPropertySection").show();
 }else{
     jQuery("#yourPropertySection").hide();
 }
});

jQuery("input[type=radio][name=haveDeposit]").change(function(){
  jQuery("#haveDepositError").hide();
  error_message = error_message.replace(ERROR_17, "");
  showErrorMessages();
});


jQuery("#apiSubmit").click(function(){

  check_loanAmount();

 if(!jQuery("#terms").prop("checked")){
    jQuery("#termsError").html("We would love to help you, but we can\'t unless you accept our privacy policy.");
    jQuery("#termsError").show();
    jQuery("#termsError").css("display","block");
    error_message = error_message.replace(ERROR_18, "");
    error_message += ERROR_18;
} else {
    error_message = error_message.replace(ERROR_18, "");  
    jQuery("#termsError").hide();
}

if(jQuery("input[name=hasProperty]").length && jQuery("input[name=hasProperty]:checked").length == 0){
 jQuery("#hasPropertyError").html("required");
 jQuery("#hasPropertyError").show();
 error_message = error_message.replace(ERROR_1, "");
 error_message += ERROR_1;
}

if(jQuery("input[name=haveDeposit]").length && jQuery("input[name=haveDeposit]:checked").length == 0){
 jQuery("#haveDepositError").html("required");
 jQuery("#haveDepositError").show();
 error_message = error_message.replace(ERROR_17, "");
 error_message += ERROR_17;
}

check_PropertyValues();


if(jQuery("#typeOfLoan").length) check_typeOfLoan();
if(jQuery("#mobileNumber").length) check_mobileNumber();
if(jQuery("#emailAddress").length) check_emailAddress();
if(jQuery("#firstName").length) check_firstName();
if(jQuery("#lastName").length) check_lastName();
if(jQuery("#suburb").length) check_suburb();
if(jQuery("#postCode").length) check_postCode();
if(jQuery("#referral").length) check_referral();

if(error_message == ""){
    jQuery("#apiSubmit").prop("disabled", true);
    jQuery("#apiEnquiryForm").submit();
}else{
 showErrorMessages();
}
});

function check_loanAmount(){
    if(jQuery("#loanAmount").length && jQuery("#loanAmount").val() == ""){
        jQuery("#loanAmountError").html("required");
        jQuery("#loanAmountError").show();
        jQuery("#loanAmount").addClass("field-required");
        error_message = error_message.replace(ERROR_3, "");
        error_message += ERROR_3;
    } else {
        jQuery("#loanAmountError").hide();
        error_message = error_message.replace(ERROR_3, "");
        jQuery("#loanAmount").removeClass("field-required");
        showErrorMessages();
    }
}

function check_typeOfLoan(){
  if(jQuery("#typeOfLoan").val() == ""){
    jQuery("#typeOfLoanError").html("required");
    jQuery("#typeOfLoanError").show();
    jQuery("#typeOfLoan").addClass("field-required");
    error_message = error_message.replace(ERROR_2, "");
    error_message += ERROR_2;
  } else{
    jQuery("#typeOfLoanError").hide();
    jQuery("#typeOfLoan").removeClass("field-required");
    error_message = error_message.replace(ERROR_2, "");
    showErrorMessages();
  }
}

function check_Property() {
    if(!jQuery("#hasPropertyYes").is(":checked") && !jQuery("#hasPropertyNo").is(":checked")) {
        jQuery("#hasPropertyError").html("required");
        jQuery("#hasPropertyError").show();
        jQuery(".switch-field label:first-of-type").addClass("field-required");
        jQuery(".switch-field label:last-of-type").addClass("field-required");
        error_message = error_message.replace(ERROR_1, "");
        error_message += ERROR_1;
    } else {
        jQuery("#hasPropertyError").hide();
        jQuery(".switch-field label:first-of-type").removeClass("field-required");
        jQuery(".switch-field label:last-of-type").removeClass("field-required");
        error_message = error_message.replace(ERROR_1, "");
        showErrorMessages();
    }
}

function check_PropertyValues(checkRealStateValue = true, checkBalanceOwing = true) {
    if(jQuery("#hasPropertyYes").is(":checked")){
        if(jQuery("#realEstateValue").val() == "" && checkRealStateValue){
            jQuery("#realEstateValueError").html("required");
            jQuery("#realEstateValueError").show();
            jQuery("#realEstateValue").addClass("field-required");
            error_message = error_message.replace(ERROR_4, "");
            error_message += ERROR_4;
        } else {
            jQuery("#realEstateValueError").hide();
            jQuery("#realEstateValue").removeClass("field-required");
            error_message = error_message.replace(ERROR_4, "");
            showErrorMessages();
        }
        if(jQuery("#balanceOwing").val() == "" && checkBalanceOwing){
            jQuery("#balanceOwingError").html("required");
            jQuery("#balanceOwingError").show();
            jQuery("#balanceOwing").addClass("field-required");
            error_message = error_message.replace(ERROR_5, "");
            error_message += ERROR_5;
        } else {
            jQuery("#balanceOwingError").hide();
            jQuery("#balanceOwing").removeClass("field-required");
            error_message = error_message.replace(ERROR_5, "");
            showErrorMessages();
        }
    }
}

function check_referral(){
  if(jQuery("#referral").val() == ""){
    jQuery("#referralError").html("required");
    jQuery("#referralError").show();
    error_message = error_message.replace(ERROR_16, "");
    error_message += ERROR_16;
}else{
    jQuery("#referralError").hide();
    error_message = error_message.replace(ERROR_16, "");
    showErrorMessages();
}
}

/*function check_landLineNumber(){
    if(jQuery("#landLineNumber").val() != "" && jQuery("#landLineNumber").val().length != 8){
     jQuery("#landLineNumberError").html("invalid");
     jQuery("#landLineNumberError").show();
     error_message = error_message.replace(ERROR_14, "");
     error_message += ERROR_14;
 }else{
     jQuery("#landLineNumberError").hide();
     error_message = error_message.replace(ERROR_14, "");
 }
}*/

function check_mobileNumber(){
    if(jQuery("#mobileNumber").val() == ""){
     jQuery("#mobileNumberError").html("required");
     jQuery("#mobileNumberError").show();
     jQuery("#mobileNumber").addClass("field-required");
     error_message = error_message.replace(ERROR_6, "");
     error_message += ERROR_6;
 }else if(jQuery("#mobileNumber").val().indexOf("04") != 0 || jQuery("#mobileNumber").val().length != 10){
     jQuery("#mobileNumberError").html("invalid");
     jQuery("#mobileNumberError").show();
     jQuery("#mobileNumber").addClass("field-required");
     error_message = error_message.replace(ERROR_7, "");
     error_message += ERROR_7;
 }else{
     jQuery("#mobileNumberError").hide();
     error_message = error_message.replace(ERROR_6, "");
     error_message = error_message.replace(ERROR_7, "");
     jQuery("#mobileNumber").removeClass("field-required");
 }
}

function check_emailAddress(){
    jQuery("#emailAddress").val(jQuery("#emailAddress").val().replace(" ",""))
    if(jQuery("#emailAddress").val() == ""){
     jQuery("#emailAddressError").html("required");
     jQuery("#emailAddressError").show();
     jQuery("#emailAddress").addClass("field-required");
     error_message = error_message.replace(ERROR_8, "");
     error_message += ERROR_8;
 }else if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(jQuery("#emailAddress").val()) == false){
     jQuery("#emailAddressError").html("invalid");
     jQuery("#emailAddressError").show();
     jQuery("#emailAddress").addClass("field-required");
     error_message = error_message.replace(ERROR_9, "");
     error_message += ERROR_9;
 }else{
     jQuery("#emailAddressError").hide();
     error_message = error_message.replace(ERROR_8, "");
     error_message = error_message.replace(ERROR_9, "");
     jQuery("#emailAddress").removeClass("field-required");
 }
}

function check_firstName(){
    if(jQuery("#firstName").val() == ""){
     jQuery("#firstNameError").html("required");
     jQuery("#firstNameError").show();
     jQuery("#firstName").addClass("field-required");
     error_message = error_message.replace(ERROR_12, "");
     error_message += ERROR_12;
 }else{
     jQuery("#firstNameError").hide();
     jQuery("#firstName").removeClass("field-required");
     error_message = error_message.replace(ERROR_12, "");
 }
}

function check_lastName(){
    if(jQuery("#lastName").val() == ""){
     jQuery("#lastNameError").html("required");
     jQuery("#lastNameError").show();
     error_message = error_message.replace(ERROR_13, "");
     error_message += ERROR_13;
 }else{
     jQuery("#lastNameError").hide();
     error_message = error_message.replace(ERROR_13, "");
 }
}

function check_suburb(){
    if(jQuery("#suburb").val() == ""){
     jQuery("#suburbError").html("required");
     jQuery("#suburbError").show();
     error_message = error_message.replace(ERROR_15, "");
     error_message += ERROR_15;
 }else{
     jQuery("#suburbError").hide();
     error_message = error_message.replace(ERROR_15, "");
 }
}

function check_postCode(){
    if(jQuery("#postCode").val() == ""){
     jQuery("#postCodeError").html("required");
     jQuery("#postCodeError").show();
     jQuery("#postCode").addClass("field-required");
     error_message = error_message.replace(ERROR_10, "");
     error_message += ERROR_10;
 }else if(jQuery("#postCode").val().length != 4){
     jQuery("#postCodeError").html("invalid");
     jQuery("#postCodeError").show();
     jQuery("#postCode").addClass("field-required");
     error_message = error_message.replace(ERROR_11, "");
     error_message += ERROR_11;
 }else{
     jQuery("#postCodeError").hide();
     error_message = error_message.replace(ERROR_10, "");
     error_message = error_message.replace(ERROR_11, "");
     jQuery("#postCode").removeClass("field-required");
 }
}

function showErrorMessages(){
    if(error_message == ""){
      jQuery("#error_message").html("");
  }else{
      jQuery("#error_message").html("<b>Oops, we\'ve missed something...</b><ul>"+error_message+"</ul>");
  }
}

if(localStorage.getItem("privacy") === "accept"){
    localStorage.removeItem("privacy");
    jQuery("#apiSubmit").trigger("click");
}
})
;
</script>'
;

$contact_form .= '
<script>
    $(document).ready(function() {
                                
        var form_viewed = false;
        var EVENT_LABEL_FORM_VIEW = "Viewed";

        var form_step_1_fill_out = false;
        var EVENT_LABEL_FORM_STEP_1_FILL_OUT = "Step 1 - User Filling Out";

        var form_step_1_next = false;
        var EVENT_LABEL_FORM_STEP_1_NEXT = "Step 1 - Pressed [Get Started]";

        var form_step_2_fill_out = false;
        var EVENT_LABEL_FORM_STEP_2_FILL_OUT = "Step 2 - User Filling Out";

        var form_step_2_next = false;
        var EVENT_LABEL_FORM_STEP_2_NEXT = "Step 2 - Pressed [Next]";

        var form_step_3_fill_out = false;
        var EVENT_LABEL_FORM_STEP_3_FILL_OUT = "Step 3 - User Filling Out";

        var form_step_3_next = false;
        var EVENT_LABEL_FORM_STEP_3_NEXT = "Step 3 - Pressed [Submit]";

        if(form_viewed == false) { 
            form_viewed = true;
            trackEnquiryForm(EVENT_LABEL_FORM_VIEW);             
        }
        
        $("#loanAmount, #typeOfLoan, #hasPropertyYes, #hasPropertyNo").on("input",function(e){
            if(form_step_1_fill_out == false) { 
                form_step_1_fill_out = true;
                trackEnquiryForm(EVENT_LABEL_FORM_STEP_1_FILL_OUT);                    
            }
        });
        $(".next0").click(function() {
            if(form_step_1_next == false) { 
                form_step_1_next = true;
                trackEnquiryForm(EVENT_LABEL_FORM_STEP_1_NEXT);
            }
        });
        $("#firstName, #mobileNumber, #emailAddress").on("input",function(e){
            if(form_step_2_fill_out == false) { 
                form_step_2_fill_out = true;
                trackEnquiryForm(EVENT_LABEL_FORM_STEP_2_FILL_OUT);
            }
        });
        $(".next1").click(function() {
            if(form_step_2_next == false) { 
                form_step_2_next = true;
                trackEnquiryForm(EVENT_LABEL_FORM_STEP_2_NEXT);
            }
        });
        $("#state, #postCode, #comments, #terms").on("input",function(e){
            if(form_step_3_fill_out == false) { 
                form_step_3_fill_out = true;
                trackEnquiryForm(EVENT_LABEL_FORM_STEP_3_FILL_OUT);
            }
        });
        $("#apiSubmit").click(function() {
            if(form_step_3_next == false) { 
                form_step_3_next = true;
                trackEnquiryForm(EVENT_LABEL_FORM_STEP_3_NEXT);                    
            }
        });
        
        function trackEnquiryForm(label) {
            if(typeof gtag !== "undefined") {
                gtag("event", "click", {"event_category": "Enquiry Form", "event_label": label});                    
            }
        }	
        
    });
</script>'
;

    return $contact_form;
}


function register_alcapiform_shortcodes() {
    add_shortcode('alcapi_contact_form', 'alcapiform_shortcode_api_contact_form');
}
add_action('init', 'register_alcapiform_shortcodes');
    


//sample usage: [iframe_contact_form baseurl="http://applicationform.hatpacks.com.au/EnquiryForm/default.aspx" style="width: 90%; height: 900px;" cid="CC" svs="0" css="http://test2.cleancredit.com.au/site/wp-content/themes/cleancredit/styles/enquiry-form.css"]
//create setting page
if (is_admin()) {
    include "settings.php";
    $alcapiform_settings_page = new ALCAPIFormSettingsPage();
}

//create setting quick link
function alcapiform_plugin_action_links($links, $file) {
    static $this_plugin;

    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }

    if ($file == $this_plugin) {
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=alcapiform-setting-admin">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}

add_filter('plugin_action_links', 'alcapiform_plugin_action_links', 10, 2);

//functions
if (!function_exists("TimStartsWith")) {

    function TimStartsWith($haystack, $needle) {
        return !strncmp($haystack, $needle, strlen($needle));
    }

}

function record_lead($app, $lead) {
    
	try {
        $options = get_option('alcapiform_option_name');	
		
		$_record_leads = isset($atts['record_leads']) ? $atts['record_leads'] : $options['record_leads'];
		$_send_email_lead_fail = isset($atts['send_email_lead_fail']) ? $atts['send_email_lead_fail'] : $options['send_email_lead_fail'];
		$_email_lead_fail = isset($atts['email_lead_fail']) ? $atts['email_lead_fail'] : $options['email_lead_fail'];		
        $_ignore_customers_lead_fail = isset($atts['ignore_customers_lead_fail']) ? $atts['ignore_customers_lead_fail'] : $options['ignore_customers_lead_fail'];
        $_ignore_emails_lead_fail = isset($atts['ignore_emails_lead_fail']) ? $atts['ignore_emails_lead_fail'] : $options['ignore_emails_lead_fail'];
        
		// Internal parameters
		// TODO: Needs to be moved to settings
		
		$company = get_bloginfo('name');
		$header = array('First Name', 'Loan Amount', 'Type Of Loan', 'Mobile Number', 'Email Address', 'Platform', 'Date Time', 'Response', 'Error Message', 'Full Request');
		$status = $lead[7];
        $customer = $lead[0];
        $error_message = $lead[8];
        $customer_email = $lead[4];
		$developer_email = "boni@chillidee.com.au";
				
		// =============================================================
		
		if(isset($_ignore_customers_lead_fail)) {		

            $ignore_customers_lead_fail = explode(', ', $_ignore_customers_lead_fail);	
            		
			if(in_array($customer, $ignore_customers_lead_fail)) { //Ignore SPAM from customer
				return;
			}			
        }
        
        if(isset($_ignore_emails_lead_fail)) {		

            $_ignore_emails_lead_fail = explode(', ', $_ignore_emails_lead_fail);	
            
            foreach ($_ignore_emails_lead_fail as $ignore_email) {
                if(stristr($customer_email, $ignore_email)) { //Ignore SPAM from email                    
				    return;
                }
            }			
		}	
		
		$filepath = WP_CONTENT_DIR . '/leads/'.$app.'_'.date("Y").'_'.date("m").'_leads_log.csv';
						
		if (!file_exists(WP_CONTENT_DIR . '/leads')) {
			mkdir(WP_CONTENT_DIR . '/leads', 0777, true);
		}
		
		$file_exists = file_exists($filepath);
		$file = fopen($filepath, 'a');
	  
		if($file === FALSE) {
			wp_mail($developer_email, $company . " - Record Lead failed", 'Failed to open temporary file '.$filepath);
		} else {

			if(!$file_exists) {
				// save the column headers
				fputcsv($file, $header);
			}			 				
			
			$loan_desc = '';
			
			switch ($lead[2]) {
				case 126:
					$loan_desc = "Debt Consolidation";
					break;
				case 18:
					$loan_desc = "Refinance";
					break;
				case 17:
					$loan_desc = "Personal";
					break;
				case 2:
					$loan_desc = "Motor Vehicle";
					break;
				case 135:
					$loan_desc = "Other";
					break;
				case 175:
					$loan_desc = "Loan for New Business";
					break;
				case 176:
					$loan_desc = "Loan for Existing Business";
					break;
				case 166:
					$loan_desc = "Short Term Loans";
					break;
				case 165:
					$loan_desc = "Self Employed Loans";
					break;
				case 164:
					$loan_desc = "Private Funding";
					break;
			}

			$lead[2] = $loan_desc;
				
			// Record lead in CSV file (log)
			if($_record_leads) {
				fputcsv($file, $lead);
            }
            								
			fclose($file);
			
			// Send email if lead failed
			if($_send_email_lead_fail && $status != 'SUCCESS') {
				
				array_pop($header);
				array_pop($lead);
			
				$html = '<p>Please contact <b>'.$company.'</b> customer <b>'.$customer.'</b> who is having problems submitting his/her lead on <a href="'.home_url().'">'.home_url().'</a></p>';
				$html .= '<br/>Details below:<br/><br/>';
				$html .= '<table>
							<tr>
								<td width="150">'.implode (": <br/>", $header).'</td>
								<td width="600">'.implode ("<br/>", $lead).'</td>
							</tr>
						  </table>';
                
                // Filter out SPAM
                if(strpos($error_message, '<loanAmount> is required., <realEstateValue> is not valid., <mobileNumber> is required.') !== false || strlen($customer) > 50 || ctype_alpha(str_replace(' ', '',  $customer)) === false) {
                    // Stay away from my email notifications little cheeky bugger!
                } else {
                    wp_mail(array($developer_email, $_email_lead_fail), $company . " - Lead failed", $html);						
                }		
            }          
		}
		//}
	} catch (Exception $e) { }
}

function getUrlParams() {
    return isset($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : '/';
}